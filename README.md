# PSI1 - Vícevláknový TCP server



## Popis

Jedná se o TCP server napsaný v jazyce C pomocí BSD socket. Server přečte řetězec poslaný klientem a odpoví zprávou, ve které tento řetězec zopakuje.

## Implementační záležitosti

Při spuštění serveru lze pomocí přepínače `-p` předat číslo portu, na kterém má server běžet. Defaultní hodnotou je port 10000.

Pro každého klienta je vytvářeno samostatné vlákno.

Maximální délka řetězce od klienta je aktuálně nastavena na 1000.


## Sestavení
Sestavení  lze provést příkazem `make` v kořenovém adresáři.


## Spuštění

```
./server -p <číslo portu>
```


## Úklid

Úklid lze provést příkazem `make clean` v kořenovém adresáři.

