CC=gcc
CFLAGS=-Wall -Wextra -pedantic -ansi

all: clean server

server: server.c
	${CC} -o server server.c ${CFLAGS} -lpthread
	
clean:
	rm -f server
