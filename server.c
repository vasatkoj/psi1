#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>

#define BUFFER_SIZE 1000
#define DEF_PORT 10000
#define MIN_PORT 1
#define MAX_PORT 65535
#define BACKLOG 5

void *serve_request(void *arg) {
	char *echoStart = "You wrote:";
	int client_socket = *(int *) arg;
	ssize_t to_recv = sizeof(char);
	char buf[BUFFER_SIZE + 1] = {0};
	int length = 0;	
	char *result;

	do {
		if (length >= BUFFER_SIZE) {
			printf("String too long to fit in buffer!\n");
			break;
		}

		if (recv(client_socket, buf + length * sizeof(char), to_recv, 0) == to_recv) {
			/*printf("Dostal jsem %c\n", buf[length]);*/
		} else {
			printf("Recv ER\n");
			break;
		}
	} while (buf[length++] != '\n');

	printf("Received: %s\n", buf);	
	result = calloc(strlen(echoStart) + strlen(buf) + 1, sizeof(char));	
	strcpy(result, echoStart);
	strcpy(result + sizeof(char) * strlen(echoStart), buf);
	send(client_socket, result, strlen(result) * sizeof(char) + 1, 0);

	free(result);
 	close(client_socket);
	free(arg);
	pthread_detach(pthread_self());
 	return 0;
}

int main(int argc, char **argv) {	
	int server_socket;
	int client_socket;
	int return_value;
	int *th_socket;
	struct sockaddr_in local_addr;
	struct sockaddr_in remote_addr;
	socklen_t remote_addr_len;
	pthread_t thread_id;
	int i;
	char *arg;
	long tmp;
	int port = DEF_PORT;

	i = 1;
    while (i < argc) {
        arg = argv[i++];
		if (strcmp(arg, "-p") == 0) {
            if (i < argc) {
				char *endptr;

				errno = 0;
                tmp = strtol(argv[i], &endptr, 0);	
				if (*argv[i] == '\0' || *endptr != '\0' || errno != 0) {
					printf("Port is not a valid number\n");
					return -1;
				} else  if (tmp < MIN_PORT || tmp > MAX_PORT) {
                    printf("Port number not in range <%d, %d>\n", MIN_PORT, MAX_PORT);
					return -1;
                } else {
					port = tmp;
				}
				i++;
            } else {
				printf("%s requires port number\n", "-p");
				return -1;
			}
        } else {
			printf("Ignoring unknown argument %s\n", arg);
		}
    }
	
	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (server_socket >= 0) {
		printf("Socket OK\n");
	} else {
		printf("Socket ER\n");
		return -1;
	}
	
	memset(&local_addr, 0, sizeof(local_addr));
	local_addr.sin_family = AF_INET;
	local_addr.sin_port = htons(port);
	local_addr.sin_addr.s_addr = INADDR_ANY;
	
	return_value = bind(server_socket, (struct sockaddr *) &local_addr, sizeof(local_addr));
	if (return_value == 0) {
		printf("Bind OK\n");
	} else {
		printf("Bind ER\n");
		return -1;
	}

	return_value = listen(server_socket, BACKLOG);
	if (return_value == 0) {
		printf("Listen OK\n");
	} else {
		printf("Listen ER\n");
		return -1;
	}

	while (1) {
		memset(&remote_addr, 0, sizeof(remote_addr));		
		remote_addr_len = sizeof(remote_addr);

		client_socket = accept(server_socket, (struct sockaddr *) &remote_addr, &remote_addr_len);
		if (client_socket >= 0) {
			printf("Accept OK\n");
		} else {
			printf("Accept ER\n");
			return -1;
		}
		
		th_socket = malloc(sizeof(int));
		*th_socket = client_socket;
		return_value = pthread_create(&thread_id, NULL, serve_request, th_socket);
		if (return_value != 0) {
			printf("Pthread_create ER\n");
			return -1;
		};
	}

	return 0;
}
